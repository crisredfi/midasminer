//
//  MineBox.h
//  MidasMiner
//
//  Created by Crisredfi on 8/27/13.
//
//

#ifndef __MidasMiner__MineBox__
#define __MidasMiner__MineBox__

#include <iostream>
#include "MineObject.h"
#include "vector"

using namespace std;

class MineBox : public Object {
public:
    
    vector<vector<MineObject>> gameContent;
    MineBox();
    MineBox(Size size); // in case we need different size.
    ~MineBox();
    MineObject* objectAtPosition(int posX, int posY);
    bool check();
    Layer *layer;
    bool checkMineralMatch();
    void removeMineralFromVector(int posX, int posY);

private:
    bool isCreatingLayout;
    char* randomMineral(MineObject *mineral);
    MineObject* nextElementIndex(int posX, int posY);
    bool areMineralsEqual(MineObject* firstObject, MineObject* seccondObject);
    void removeNumOfMineralsFromOriginVector(int posX, int posY, int numElements);
    void checkMineralSelector();
    void setUpRecheckSelector();
    void reassignMatchingMinerals(int posX, int posY, int numElements, bool isHorizontal);
    Size _size;
    
};

#endif /* defined(__MidasMiner__MineBox__) */

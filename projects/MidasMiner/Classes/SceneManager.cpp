//
//  SceneManager.cpp
//  MidasMiner
//
//  Created by Crisredfi on 8/27/13.
//
//

#include "SceneManager.h"
#include "MainMenuLayer.h"
#include "MainScene.h"

// each method will change the visual scene layer to a new one.

void SceneManager::goMenu() {
    Layer *layer = MainMenuLayer().create();
    SceneManager::go(layer);
}

void SceneManager::goPlay() {
    
    Layer *layer = MainScene().create();
    SceneManager::go(layer);
}

void SceneManager::go(Layer* layer) {
    
    Director *director = Director::getInstance();
    Scene *newScene = SceneManager::wrap(layer);
    if (director->getRunningScene()) {
        director->replaceScene(newScene);
    } else {
        director->runWithScene(newScene);
    }
}

Scene* SceneManager::wrap(Layer* layer) {
    Scene *newScene = Scene::create();
    newScene->addChild(layer);
    return newScene;
    
    
}


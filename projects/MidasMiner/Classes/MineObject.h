//
//  MineObject.h
//  MidasMiner
//
//  Created by Crisredfi on 8/27/13.
//
//

#ifndef __MidasMiner__MineObject__
#define __MidasMiner__MineObject__

#include <iostream>
#include "cocos2d.h"
using namespace cocos2d;

enum MineObjectType {
    Blue ,
    Green,
    Red,
    Yellow,
    Purple,
    Default = Blue
};


class MineObject : public Object {
  
    public :
    MineObject();
    MineObject(int posX, int posY);
    ~MineObject();
    int x;
    int y;
    Sprite *sprite;
    
    MineObjectType mineralType;
    Point pixPosition();
    Point topPosition();
    Point newMinePositionFromHeigh(int height);
    bool nearTile(MineObject* otherTile);
    void trade(MineObject* otherTile);

private:
    
};

#endif /* defined(__MidasMiner__MineObject__) */

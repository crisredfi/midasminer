//
//  SceneManager.h
//  MidasMiner
//
//  Created by Crisredfi on 8/27/13.
//
// this class will be only usable in case we create a menu to handle different scenes


#ifndef __MidasMiner__SceneManager__
#define __MidasMiner__SceneManager__

#include <iostream>
#include "MainScene.h"

class SceneManager : Object {
    
public:
    static void goMenu();
    static void goPlay();
    static void go(Layer* layer);
    static Scene* wrap(Layer* layer);
};

#endif /* defined(__MidasMiner__SceneManager__) */

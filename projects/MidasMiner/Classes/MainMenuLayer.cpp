//
//  MainMenuLayer.cpp
//  MidasMiner
//
//  Created by crisredfi on 9/2/13.
//
//

#include "MainMenuLayer.h"
#include "SceneManager.h"


Scene* MainMenuLayer::scene() {
    //cocos 2dx handles autorelease, so no memory managemnt needed her
    
    Scene *scene = Scene::create();
    MainMenuLayer *layer = MainMenuLayer::create();
    scene->addChild(layer);
    return scene;
    
    
}


bool MainMenuLayer::init() {
    
    if (!Layer::init()) {
        return false;
    }
    
    Size winSize = Director::getInstance()->getWinSize();
    Sprite *background = Sprite::create("miner_764x260.jpg");
    background->setPosition(Point(winSize.width/2, winSize.height/2));
    background->setScale(winSize.width/background->boundingBox().size.width);

    this->addChild(background);
    
    MenuItemImage *startnew = MenuItemImage::create("new_game_button.png",
                                                    "new_game_button_down.png", CC_CALLBACK_1(MainMenuLayer::onNewGame, this));
    
    //startnew->setScale(1.5);
    Menu *menu = Menu::create(startnew, NULL);
    menu->setPosition(Point(winSize.width - winSize.width/3, winSize.height/2));
    menu->alignItemsVerticallyWithPadding(20.0f);
    this->addChild(menu);
    
    return true;
}

void MainMenuLayer::onNewGame(Object* pSender) {
    SceneManager::goPlay();
    
}

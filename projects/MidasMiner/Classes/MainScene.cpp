//
//  MainScene.cpp
//  MidasMiner
//
//  Created by Crisredfi on 8/27/13.
//
//

#include "MainScene.h"
#include "Constants.h"
#include "SceneManager.h"

Scene* MainScene::scene()
{
    // 'scene' is an autorelease object
    Scene *scene = Scene::create();
    
    // 'layer' is an autorelease object
    MainScene *layer = MainScene::create();
    
    // add layer as a child to scene
    scene->addChild(layer);
    
    // return the scene
    return scene;
}


// on "init" you need to initialize your instance
bool MainScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Layer::init() )
    {
        return false;
    }
    
    Size visibleSize = Director::getInstance()->getVisibleSize();
    Point origin = Director::getInstance()->getVisibleOrigin();
    // Most of Cocos2dx objects are autoreleasable, so no memory
    // handling is necessary except rare cases. 
    Sprite* sprite = Sprite::create("BackGround.jpg");
    // position the sprite on the center of the screen
    sprite->setPosition(Point(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
    
    // add the sprite as a child to this layer
    this->addChild(sprite, 0);

    box = new MineBox(Size(5, 5));
    box->layer = this;
    box->check();

    this->setTouchEnabled(true);
    countdown = kCountdownTimer; // 60 secconds time
    this->schedule(schedule_selector(MainScene::update), 1.0f);
    LabelTTF *solvedLabel = LabelTTF::create("Time Left 1:00", "Marker Felt", 32);
    Size winSize = Director::getInstance()->getWinSize();
	solvedLabel->setAnchorPoint(Point(0,0));
	solvedLabel->setPosition(Point(kClockWidthOffset, winSize.height - kClockHeightOffset));
    this->addChild(solvedLabel, kSolvedText, kSolvedText);
    
    LabelTTF *pointsLabel = LabelTTF::create("Points: 0", "Marker Felt", 32);
	pointsLabel->setAnchorPoint(Point(0,0));
	pointsLabel->setPosition(Point(kClockWidthOffset, winSize.height - kClockHeightOffset - 40));
    this->addChild(pointsLabel, kPuntuation, kPuntuation);
    
    return true;
}

MainScene::~MainScene() {
//cocos2dx is handling automatically the deletes. no need to delete cocos2d Objects
}


void MainScene::update(float delta) {
    
    LabelTTF *solvedLabel = (LabelTTF*)this->getChildByTag(kSolvedText);
    this->countdown--;
    char str[20] = {0};
    string extra = "";
    if (this->countdown < 10) {
        extra = "0";
    }
    sprintf(str, "Time Left 0:%s%.f", extra.c_str(), this->countdown );
    solvedLabel->setString( str );
    if (this->countdown <= 0) {
        // finish the game.
        LabelTTF *puntuationLabel = (LabelTTF*)this->getChildByTag(kPuntuation);
        Size visibleSize = Director::getInstance()->getVisibleSize();
        
        solvedLabel->setPosition(Point(visibleSize.width/2 -solvedLabel->getContentSize().width/2 ,
                                       visibleSize.height/2));
        solvedLabel->setScale(3);
        solvedLabel->setColor(Color3B::BLUE);
        solvedLabel->setString("Game OVER");
        this->unscheduleAllSelectors();
        this->setTouchEnabled(false);
        MenuItemImage *backButton = MenuItemImage::create("back_button.png",
                                                          "back_button_down.png", CC_CALLBACK_1(MainScene::backToMainMenu, this));
        
        //startnew->setScale(1.5);
        Menu *menu = Menu::create(backButton, NULL);
        menu->setPosition(Point(puntuationLabel->getPosition().x + backButton->getBoundingBox().size.width/2,
                                puntuationLabel->getPosition().y - backButton->getBoundingBox().size.height/2));
        menu->alignItemsVerticallyWithPadding(20.0f);
        this->addChild(menu);
        
    }
}


void MainScene::backToMainMenu(Object* pSender) {
    SceneManager::goMenu();

}

void MainScene::ccTouchesBegan(Set *touches, Event *event) {
    
    Touch *touch = (Touch*)touches->anyObject();
    Point location = touch->getLocationInView();
    
    location = Director::getInstance()->convertToGL(location);

    if (location.y < (kStartY) || location.y > (kStartY + (kTileSize * kBoxHeight))) {
		return;
	}
	
	int x = (location.x - kStartX) / (kTileSize);
	int y = (location.y - kStartY) / (kTileSize);
	
	if (selectedMine && selectedMine->x == x && selectedMine->y == y) {
		selectedMine = 0;
		return;
	}
    
    MineObject *mine = box->objectAtPosition(x, y);
    
    if (mine->x >=0 && mine->y >=0) {
        if (selectedMine && selectedMine->nearTile(mine)) {
            this->changeTilesWithSelector(selectedMine, mine);
        } else {
            if (selectedMine) {
                if (selectedMine->x == x && selectedMine->y == y) {
                    selectedMine = 0;
                }
            }
            selectedMine = mine;
        }
    }
}




void MainScene::changeTilesWithSelector(MineObject *a, MineObject* b) {
    
    movedMine = b;
    Action *actionA = MoveTo::create(kMoveTileTime, b->pixPosition());    
    Action *actionB = MoveTo::create(kMoveTileTime, a->pixPosition());
    
    a->sprite->runAction(actionA);
    b->sprite->runAction(actionB);
    a->trade(b);
    
    DelayTime *delayAction = DelayTime::create(kMoveTileTime + 0.1f);
    // perform the selector call
    // deprecated method in cocos2dx 3.0. but since is still a beta, moving to c++ 11
    // and still having some issues im using the old method. 
    CallFunc *callSelectorAction = CallFunc::create(this, callfunc_selector(MainScene::checkMineral));
    // run the action
    this->runAction(Sequence::create(delayAction,
                                        callSelectorAction,
                                        NULL));
    
}

void MainScene::checkMineral() {
    
    bool isSolved = box->checkMineralMatch();
    // when we have a match, since we can match horizontal and perpendicular
    // matches, we handle the code in MineBox class. if not, then we
    // move back the Tiles to the previous position
       if (!isSolved) {
        
        movedMine->sprite->runAction(Sequence::create(DelayTime::create(kMoveTileTime + 0.1f),
                                                      MoveTo::create(kMoveTileTime, selectedMine->pixPosition()), NULL));
        selectedMine->sprite->runAction(Sequence::create(DelayTime::create(kMoveTileTime + 0.1f),
                                                         MoveTo::create(kMoveTileTime, movedMine->pixPosition()), NULL));
        
        selectedMine->trade(movedMine);
    }
    // nullify pointers.
    selectedMine = 0;
    movedMine = 0;

}


void MainScene::updatePointsWithNumber(int newPoints) {
    
    LabelTTF *pointsLabel = (LabelTTF*)this->getChildByTag(kPuntuation);

    char str[10] = {0};
    this->points += newPoints;
    sprintf(str, "Points: %.f", this->points );
    pointsLabel->setString( str );

}


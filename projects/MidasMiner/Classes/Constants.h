//
//  Constants.h
//  MidasMiner
//
//  Created by Crisredfi on 8/28/13.
//
//

#ifndef MidasMiner_Constants_h
#define MidasMiner_Constants_h

#define CC_IS_IPAD() (Director::getInstance()->getWinSize().width==1024||Director::getInstance()->getWinSize().height==1024)
#define CC_IS_IPAD3() (CC_IS_IPAD() && CC_CONTENT_SCALE_FACTOR()>1.0f)
#define kiphoneToIpadFactor ((CC_IS_IPAD3())?2:1)
#define kTileSize 42.0f
#define kMoveTileTime 0.2f
#define kBoxWidth 8
#define kBoxHeight 8
#define kStartX (460*Director::getInstance()->getWinSize().width)/1024
#define kStartY 250*Director::getInstance()->getWinSize().height/762
#define kSolvedText 125
#define kPuntuation 126
#define kClockHeightOffset 140
#define kClockWidthOffset 140
#define kCountdownTimer 60

#endif

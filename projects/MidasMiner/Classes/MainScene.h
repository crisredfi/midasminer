//
//  MainScene.h
//  MidasMiner
//
//  Created by Crisredfi on 8/27/13.
//
//

#ifndef __MidasMiner__MainScene__
#define __MidasMiner__MainScene__

#include <iostream>
#include "cocos2d.h"
#include "MineBox.h"

using namespace cocos2d;

class MainScene : public Layer {
    
private:
    MineObject *selectedMine;
    MineObject *movedMine;
    void changeTilesWithSelector(MineObject *a, MineObject* b);
    float countdown;
    float points;
    void backToMainMenu(Object* pSender);
    
public:
    
    ~MainScene();
    virtual void update(float delta);
    virtual bool init();
    static Scene* scene();
    CREATE_FUNC(MainScene);
    void checkMineral();
    MineBox *box;
    virtual void ccTouchesBegan(Set *touches, Event *event);
    void updatePointsWithNumber(int newPoints);
};

#endif /* defined(__MidasMiner__MainScene__) */

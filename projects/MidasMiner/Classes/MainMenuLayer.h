//
//  MainMenuLayer.h
//  MidasMiner
//
//  Created by crisredfi on 9/2/13.
//
//

#ifndef __MidasMiner__MainMenuLayer__
#define __MidasMiner__MainMenuLayer__

#include <iostream>

#include "cocos2d.h"

using namespace cocos2d;

class MainMenuLayer : public Layer {
    
public:
    virtual bool init();
    static Scene* scene();
    // a selector callback
    void menuCloseCallback(Object* pSender);
    
    CREATE_FUNC(MainMenuLayer);
    
private:
    void onNewGame(Object* pSender);
    void onOptions(Object* pSender);
    void onCredits(Object* pSender);
    
    
};


#endif /* defined(__MidasMiner__MainMenuLayer__) */

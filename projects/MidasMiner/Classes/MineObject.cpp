//
//  MineObject.cpp
//  MidasMiner
//
//  Created by Crisredfi on 8/27/13.
//
//

#include "MineObject.h"
#include "Constants.h"

MineObject::MineObject():Object() {
    
    y = 0;
    x = 0;
    sprite = 0;
    mineralType = Default;
    
}


MineObject::MineObject(int posX, int posY):Object() {
    x = posX;
    y = posY;
    sprite = 0;
    mineralType = Default;
    
}


MineObject::~MineObject() {
    
}

Point MineObject::pixPosition() {
    
    return Point(kStartX + x * kTileSize +kTileSize/2.0f,
                 kStartY + y * kTileSize +kTileSize/2.0f);
}

Point MineObject::topPosition() {
    return Point(kStartX + x * kTileSize +kTileSize/2.0f,
                 kStartY + kBoxHeight * kTileSize +kTileSize/2.0f);
}

Point MineObject::newMinePositionFromHeigh(int height) {
    return Point(kStartX + x * kTileSize +kTileSize/2.0f,
                 (kTileSize * height) + kStartY + kBoxHeight * kTileSize + kTileSize/2.0f);
}

bool MineObject::nearTile(MineObject* otherTile) {
    return (x == otherTile->x && abs(y - otherTile->y)==1) ||
    (y == otherTile->y && abs(x - otherTile->x)==1);
}


void MineObject::trade(MineObject* otherTile) {
    
    Sprite *tempSprite = sprite;
    MineObjectType tempValue = mineralType;
    sprite = otherTile->sprite;
    mineralType = otherTile->mineralType;
    otherTile->sprite = tempSprite;
    otherTile->mineralType = tempValue;
    
}


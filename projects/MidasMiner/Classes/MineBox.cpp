//
//  MineBox.cpp
//  MidasMiner
//
//  Created by Crisredfi on 8/27/13.
//
//

#include "MineBox.h"
#include "Constants.h"
#include "MainScene.h"

MineBox::MineBox():Object() {
    
}

MineBox::MineBox(Size sz):Object() {
    
    _size = sz;
    // initialize randomizer
    srand((unsigned)time(0));
    
    for (int y =0; y < kBoxWidth; y++) {
        vector<MineObject> rowContent;
        for(int x = 0; x < kBoxHeight; x++) {
            // cocos 2d objecgts are autoreleaseable

            MineObject *newMineral = new MineObject(x,y);
            newMineral->sprite = 0;
            rowContent.push_back(*newMineral);
        }
        gameContent.push_back(rowContent);
    }
    this->isCreatingLayout = true;
}


MineBox::~MineBox() {
       
}

MineObject* MineBox::objectAtPosition(int posX, int posY) {
    
    if (posX < 0 || posX >= kBoxWidth || posY < 0 || posY >= kBoxHeight) {
        // something whent wrong, we are out of bounds
        log("Something whent wrong while looking for an objet");
    }
    MineObject *mineral = &gameContent[posY][posX];
    return mineral;
}


// set up all the box with all the elements of the game box
// this method creates all the box sprites and sets them in the correct position
bool MineBox::check() {
    
	for (int x=0; x< kBoxHeight; x++) {
        
		int extension = 0;
		for (int y=0; y < kBoxWidth; y++) {
            
			MineObject *tile = this->objectAtPosition(x, y);
			if(tile->sprite == 0){
				extension++;
			}else if (extension == 0) {
                cout << "position x " << x << " and position y " << y
                << " is been skipped" << endl;
			}
		}
        
		for (int i=0; i < extension; i++) {
            
			MineObject *destTile = this->objectAtPosition(x, kBoxHeight-extension+i);
            char* randomMineral = this->randomMineral(destTile);
            Sprite* sprite = Sprite::create(randomMineral);
            delete randomMineral;
			sprite->setPosition(Point(kStartX + x * kTileSize + kTileSize/2,
                                      kStartY + (kTileSize/2) + i
                                      *kTileSize));
            
            layer->addChild(sprite, 100);
			destTile->sprite = sprite;
		}
	}
	isCreatingLayout = true;
    this->checkMineralMatch();
    return isCreatingLayout;
}


// helper to create Mineral sprites. assign the sprite and the MineObjectType
char* MineBox::randomMineral(MineObject *mineral) {
    
    MineObjectType provisionalType = Default;
    if (mineral->mineralType != 0) {
        provisionalType = mineral->mineralType;
    }
    string name = "" ;
    do {
        int random_integer = (rand()%5)+1;
        
        
        switch (random_integer) {
            case 1:
                name = "Blue.png";
                mineral->mineralType = Blue;
                break;
            case 2:
                name = "Yellow.png";
                mineral->mineralType = Yellow;
                break;
            case 3:
                name = "Green.png";
                mineral->mineralType = Green;
                break;
            case 4:
                name = "Red.png";
                mineral->mineralType = Red;
                break;
            case 5:
                name = "Purple.png";
                mineral->mineralType = Purple;
                break;
            default:
                break;
        }

    } while (provisionalType == mineral->mineralType);
    
    
    char * S = new char[name.length() + 1]; // keep track of this memory
    std::strcpy(S, name.c_str());
    // returning memory allocated. we must delete it after use.
    return S;
}


bool MineBox::checkMineralMatch() {
    bool status = false;
    for (int i = 0; i < kBoxHeight; i++) {
        // check vertical matches
        for (int v=0; v < kBoxWidth; v++) {
            MineObject *mineral = &gameContent[v][i];
            int counter = 1;
            
            while (v+counter <= kBoxWidth) {
                MineObject *nextMin = this->nextElementIndex(v+counter, i);
                // random crash in this line. I guess is because some elemnts are still
                // beeing created or modified and makes the app crash since the element
                // is still not created. 
                if (this->areMineralsEqual(mineral, nextMin)) {
                    // a match has occurred, we need now to increase the counter and
                    // recheck if we reach more than 3 equal elements
                    counter++;
                } else if (counter > 2) {
                    // remove a block from Y,
                    if (isCreatingLayout) {
                        this->reassignMatchingMinerals(v, i, counter, true);
                    } else {
                        this->removeNumOfMineralsFromOriginVector(v, i, counter);
                        this->setUpRecheckSelector();
                        status = true;
                    }
                    // if we are creating the layout, reseting minerals is in
                    // progress. we want to go through all minerals to try to
                    // avoid matches. no return tipe until the end of the method
                    // is necessary
                    if (!isCreatingLayout) {
                        return true;
                    }
                    break;
                } else {
                    // no mathch, leave the while and recheck next elements
                    break;
                }
            }
        }
        // check horizontal matches
        for (int z = 0; z <= kBoxWidth; z++) {
            MineObject *mineral = &gameContent[i][z];
            int counter = 1;
            while (z+counter <= kBoxWidth) {
                MineObject *nextMin = this->nextElementIndex(i, z+counter);
                if (this->areMineralsEqual(mineral, nextMin)) {
                    // a match has occurred, we need now to increase the counter and
                    // recheck if we reach more than 3 equal elements
                    counter++;
                } else if (counter > 2) {
                    for (int count = 0; count < counter ; count++) {
                        if (isCreatingLayout) {
                            
                            this->reassignMatchingMinerals(i, z+count, 1, false);
                        } else {
                            this->removeMineralFromVector(i, z+count);
                        }
                        
                    }
                  
                    // if we are creating the layout, reseting minerals is in
                    // progress. we want to go through all minerals to try to
                    // avoid matches. no return tipe until the end of the method
                    // is necessary
                    if (!isCreatingLayout) {
                        this->setUpRecheckSelector();
                        return true;
                    }
                    break;
                } else {
                    // no mathch, leave the while and recheck next elements
                    break;
                }
                
            }
        }
    }
    this->isCreatingLayout = false;

    return status;
}

void MineBox::setUpRecheckSelector() {
    // wait untill all animations are done so we dont destroy the grid layout
    DelayTime *delayAction = DelayTime::create(kMoveTileTime *3);
    // perform the selector call
    // deprecated method in cocos2dx 3.0. but since is still a beta, Im ignoring
    // this deprecation.
    CallFunc *callSelectorAction = CallFunc::create(this, callfunc_selector(MineBox::checkMineralSelector));
    // run the action
    layer->runAction(Sequence::create(delayAction,
                                      callSelectorAction,
                                      NULL));
    
}


// helper for the function callback
void MineBox::checkMineralSelector() {
    this->checkMineralMatch();
}

bool MineBox::areMineralsEqual(MineObject* firstObject, MineObject* seccondObject) {
    try {
        if (firstObject != 0 && seccondObject != NULL) {
            return (firstObject->mineralType == seccondObject->mineralType);
            
        }
    } catch (int e) {
        log("error sending a nil object. return a false to avoid crashing");
    }
    
    return true;
}


MineObject* MineBox::nextElementIndex(int posX, int PosY) {
    
    return &gameContent[posX][PosY];
}


// set up box. in case some minerals are equal before we start playing, this method
// should change the value to a new one. 
void MineBox::reassignMatchingMinerals (int posX, int posY, int numElements, bool isHorizontal) {
    
    for (int i = 0; i < numElements; i ++)  {
        MineObject *mineral = 0;
        if (isHorizontal) {
            mineral = &gameContent[posX + i][posY];
        } else {
            mineral = &gameContent[posX][posY + i];
        }
        
        char* randomMineral = this->randomMineral(mineral);
        Sprite* sprite = Sprite::create(randomMineral);
        delete randomMineral;
        Point position = mineral->sprite->getPosition();
        if (mineral->sprite != 0) {
            mineral->sprite->removeFromParent();
        }
        mineral->sprite = sprite;
        mineral->sprite->setPosition(position);
        layer->addChild(mineral->sprite, 100);
    }
    
}




//remove element and collapse all other elemnts.
void MineBox::removeMineralFromVector(int posX, int posY) {
    
    MineObject *last = &gameContent[posX][posY];
    ((MainScene*) layer)->updatePointsWithNumber(1);

    for (int i = posX; i < kBoxHeight; i++) {
        
        MineObject *mineral = &gameContent[i][posY];        
        last->sprite->runAction(MoveTo::create(kMoveTileTime,mineral->pixPosition()));
        mineral->sprite->runAction(MoveTo::create(kMoveTileTime, last->pixPosition()));
        
        mineral->trade(last);
        last = mineral;
        mineral = 0;
        
    }
    char* randomMineral = this->randomMineral(last);
    Sprite* sprite = Sprite::create(randomMineral);
    delete randomMineral;
    
    if (last->sprite) {
        
        last->sprite->removeFromParent();
        last->sprite = sprite;
        last->sprite->setPosition(last->topPosition());
        layer->addChild(last->sprite, 100);
        last->sprite->runAction(MoveTo::create(kMoveTileTime, last->pixPosition()));        
    }
    
}

void MineBox::removeNumOfMineralsFromOriginVector(int posX, int posY, int numElements) {
    
    MineObject *last = 0;
    ((MainScene*) layer)->updatePointsWithNumber(numElements);
    int index = 1;
    for (int i = posX; i < kBoxHeight; i++) {
        // last cant be set as mineral before we remove all the
        // minerals that have a match
        if (index <= numElements) {
            last = &gameContent[i][posY];
            
            char* randomMineral = this->randomMineral(last);
            Sprite* sprite = Sprite::create(randomMineral);
            delete randomMineral;
            
            if (last->sprite != 0) {
                last->sprite->removeFromParent();
                last->sprite = sprite;
                last->sprite->setPosition(last->newMinePositionFromHeigh(numElements));
                layer->addChild(last->sprite, 100);
                last = 0;
            }
            index++;
        }
        
    }
    
    for (int i = posX; i < kBoxHeight; i++) {
        if (i < kBoxHeight - numElements) {
            
            MineObject *mineral = &gameContent[i+numElements][posY];
            last = &gameContent[i][posY];
            mineral->sprite->runAction(MoveTo::create(kMoveTileTime, last->pixPosition()));
            mineral->trade(last);
            mineral->sprite->setOpacity(255);
            // nullify pointer 
            mineral = 0;
            last = 0;
        } else {
            MineObject *mineral = &gameContent[i][posY];
            mineral->sprite->runAction(MoveTo::create(0.25f , mineral->pixPosition()));
            
        }
    }
}


